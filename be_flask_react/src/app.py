from flask import Flask, request, jsonify
from flask_pymongo import PyMongo, ObjectId

# from pymongo import MongoClient
from flask_cors import CORS

app = Flask(__name__)

# on the following line we declare the connection with mongo
# app.config['MONGO_URI'] = 'mongodb://localhost/python-react-db'
# replace user and password with your own 
db_uri = 'mongodb+srv://<user>:<password>@cluster0-2v7hx.mongodb.net/test?retryWrites=true&w=majority'
app.config['MONGO_URI'] = db_uri

mongo = PyMongo(app)

# is like a middleware, settings
CORS(app)

# db
db = mongo.db.users

# Routes
@app.route('/')
def index():
    return '<h1>Hello World</h1>'

# user for the example

# create users
@app.route('/users',methods=['POST'])
def createUsers():
    oid = db.insert_one({
        'name':request.json['name'],
        'email':request.json['email'],
        'password':request.json['password']
    }).inserted_id

    print(request.json)
    # print (str(ObjectId(str(oid))))
    return jsonify(str(ObjectId(oid)))

# get whole users 
@app.route('/users',methods=['GET'])
def getUsers():
    users = []
    for doc in db.find():
        users.append({
            '_id': str(ObjectId(doc['_id'])),
            'name': doc['name'],
            'email': doc['email'],
            'password': doc['password']

        })

    return jsonify(users)

# get just one user by id
@app.route('/users/<uid>',methods=['GET'])
def getUser(uid):
    user = db.find_one({'_id': ObjectId(uid)})
    print (user)
    return jsonify({
        '_id': str(ObjectId(user['_id'])),
        'name': user['name'],
        'email': user['email'],
        'password': user['password']
    })

# delete one user by id
@app.route('/users/<uid>',methods=['DELETE'])
def deleteUsers(uid):
    db.delete_one({'_id': ObjectId(uid)})
    return jsonify({'msg':'user deleted'})
    

# update user by id
@app.route('/users/<uid>',methods=['PUT'])
def updateUsers(uid):
    db.update_one({'_id': ObjectId(uid)},{
        '$set':{
            'name':request.json['name'],
            'email':request.json['email'],
            'password':request.json['password']
        }})
    return jsonify({'msg':'user updated'})



if __name__ == "__main__":
    app.run(debug=True)