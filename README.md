
Small project

## Flask and ReactJs CRUD
Testing program using Python and Flask and React.js 

This project was created using [React App](https://github.com/facebook/create-react-app) for the Front-end and [Flask](https://flask.palletsprojects.com/en/1.1.x/) for the Back-end following a youtube video tutorial by [Fazt](https://www.youtube.com/watch?v=D1W8H4Rkb9A&t=13s)



### `virtual env`
Turn on the virtual env in python and run the app to get the server ON

### `python src/app.py`
Runs the python virtual server in the development mode <br/> to open  [http://localhost:5000](http://localhost:5000) to view in browser

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.



<a href="https://github.com/FaztWeb/flask-react-hooks-crud">codigo fazt en git</a>



