import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Header from './components/Header'
import About from './components/About'
import Users from './components/Users'

// import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Header/>
        <div className='container p-4'>
          <Switch>
            <Route exact path='/' component={Users}/>
            <Route path='/about' component={About}/>
          </Switch>
        </div> 
      </Router>
    </div>
  );
}

export default App;
