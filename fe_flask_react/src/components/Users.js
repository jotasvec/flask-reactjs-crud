import React, { useState, useEffect } from 'react'

// calling API from .env
const API = process.env.REACT_APP_API



 const Users = () => {
    

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [id, setID] = useState('');

    const [users, setUsers] = useState([]);

    const [edit, setEdit] = useState(false);

    const fetching = async (method) => {
        const uri = !edit ? `${API}/users` : `${API}/users/${id}`
        console.log(uri)
        const res = await fetch(uri, {
                method: method,
                headers:{
                    'Accept': 'application/json',
                    'Content-Type':'application/json'},
                body: JSON.stringify({
                    name,
                    email,
                    password
                })
            }).catch(error => {
                console.log('error > '+error)
            })
            
            return await res.json();
}


    const handleSubmit = async e => {
        e.preventDefault()
        // console.log(e)
        console.log(name, email, password)
        let data 
        if(!edit){
            data = await fetching('POST')
            getUsers();

        }else{
            data = await fetching('PUT')
            setEdit(false)
            setID('')
            getUsers();
        }

        console.log(data);
        
        setName('')
        setEmail('')
        setPassword('')

    }


    const getUsers = async () =>{
        const res = await fetch(`${API}/users`);
        const data = await res.json();
        // console.log(data);
        setUsers(data);
    }

    // delete user
    const deleteUser = async (id) => {
        console.log('usuario a eliminar > '+ id)
        // const res = 
        await fetch(`${API}/users/${id}`,
            {
                method: 'DELETE',
                headers:{
                    'Accept': 'application/json',
                    'Content-Type':'application/json'}
            })
            .then(res => res.json())
        // await res.json();
        // console.log(data)
        getUsers();
    }
    // update User
    const updateUser = (user) => {
        // console.log('usuario a modificar > '+ id)
        setEdit(true)
        // const res = await fetch(`${API}/users/${id}`)
        // const data = await res.json();
        console.log("usuario e email"+user.name, user.email)

        setName(user.name)
        setEmail(user.email)
        setPassword(user.password)
        setID(user._id)

        // await getUsers();
    }

    // useEffect is like componentDidMount
    useEffect(()=>{
        getUsers();
    },[])

    // const handleChangeName = (e) => {
    //     setName(e.target.value)
    // }

    return(
        <div className='row'>
            <div className='col-md-4'>
                <form onSubmit={handleSubmit} className='card card-body'>
                    {/* name */}
                    <div className="form-group">
                        <input 
                            className='form-control'
                            type="text" 
                            onChange={(e) => setName(e.target.value)} 
                            // onChange={handleChangeName} 
                            value={name}
                            placeholder='names'
                            autoFocus

                        />
                    </div>
                    {/*email */}
                    <div className="form-group">
                        <input 
                            className='form-control'
                            type="email" 
                            // onChange={handleChangeEmail} 
                            onChange={e => setEmail(e.target.value)} 
                            value={email}
                            placeholder='email'

                        />
                    </div>
                    {/* password */}
                    <div className="form-group">
                        <input 
                            className='form-control'
                            type="password" 
                            onChange={e => setPassword(e.target.value)} 
                            value={password}
                            placeholder='password'

                        />
                    </div>
                    <button className='btn btn-primary btn-block'>
                        {!edit? 'Create user': 'Edit'}
                    </button>
                </form>
            </div>
            <div className="col-md-8">
                <table className='table table-hover table-striped'>
                    <thead>
                        <tr className='text-light bg-dark'>
                            <td>Name</td>
                            <td>Email</td>
                            <td>Password</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map(user => (
                            <tr key={user._id}>
                                <td>{user.name}</td>
                                <td>{user.email}</td>
                                <td>{user.password}</td>
                                <td> <button className='btn btn-info' onClick={() => updateUser(user)}>edit</button>  </td>
                                <td> <button className='btn btn-danger' onClick={() => deleteUser(user._id)}>delete</button> </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            
        </div>
    )
    
}

export default Users;