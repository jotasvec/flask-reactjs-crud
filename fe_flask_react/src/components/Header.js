import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => {
    return(
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <Link className="navbar-brand" to='/'>React+Python</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav">
                        {/* <Link className="nav-item nav-link active" >Home <span className="sr-only">(current)</span></Link> */}
                        <Link className="nav-item nav-link" to='/'>Users</Link>
                        <Link className="nav-item nav-link" to='/about'>About</Link>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default Header