import React, { Fragment, useState, useEffect } from 'react'
import Markdown from 'markdown-to-jsx'


const content =  require('./../README.md');

// import {markdown} from ''
 function About(){
    const [md, setMD] = useState('')
  
    useEffect(() => {
        fetch(content)
            .then(res => res.text())
            .then(text => setMD(text))
            .catch(err => setMD(err))
    })

    
    return(
        <Fragment>
            <h1>About</h1>
            <Markdown>{md}</Markdown>

        </Fragment>
        // <renderingMD/>
    );
    
}

export default About